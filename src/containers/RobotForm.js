import React, { Component } from 'react'
import Form from '../components/Form'
import PropTypes from 'prop-types';

import { userStringToRobotsData } from '../utils/io'

class RobotForm extends Component {
  static propTypes = {
  	handleSubmit: PropTypes.func.isRequired
  }

  handleSubmit = (formValues) => {
  	try{
  		this.props.handleSubmit(userStringToRobotsData(formValues.instructions.value))
  	} catch(ex) {
  		this.props.handleSubmit(
  			{
  				error: true,
  				message: ex
  			}
  		)
  	}
  }

  render() {
  	return <Form
  		handleSubmit={this.handleSubmit}
  	/>
  }
}

export default RobotForm
