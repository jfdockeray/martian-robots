import React, { Component } from 'react'
import { Well } from 'react-bootstrap'
import PropTypes from 'prop-types';

import { moveRobots } from '../utils/robot'

class RobotOutput extends Component {
  static propTypes = {
  	robots: PropTypes.array,
  	bounds: PropTypes.shape({
  		x: PropTypes.number,
  		y: PropTypes.number
  	})
  }

  state = {
  	robotOutputs: []
  }

  componentDidMount () {
  	const {
  		robots,
  		bounds
  	} = this.props
  	this.setState({
  		robotOutputs: moveRobots(robots, { bounds })
  	})
  }

  render () {
  	const {
  		robotOutputs
  	} = this.state

  	return robotOutputs.map((robotOutput, i)=>{
  		return <Well bsSize="large" key={robotOutput}>
    		{robotOutput}
    	</Well>
  	})
  }
}

export default RobotOutput
