import React from 'react'
import { Grid, Row, Col, PageHeader } from 'react-bootstrap'

const AppWrapper = ({ children }) => {
	return <Grid>
		<Row>
			<PageHeader>
          Mars Robots <small>Mission Control</small>
			</PageHeader>
		</Row>
		<Row className="show-grid">
			<Col xs={12} md={8}>
				{ children }
			</Col>
		</Row>
	</Grid>
}

export default AppWrapper
