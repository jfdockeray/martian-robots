import React, { Component } from 'react'
import { FormGroup, ControlLabel, FormControl, Button, HelpBlock} from 'react-bootstrap'
import PropTypes from 'prop-types';
import { compact } from 'lodash'

import { memoize } from 'lodash'

const validations = {
	instructions: [{
		name: 'length',
		min: 2,
		max: 100,
		message: 'An instruction string has to be at least 2 characters (to set the robots inital state) and can not be longer then 100 characters'
	}]
}

class Form extends Component {
  static propTypes = {
  	handleSubmit: PropTypes.func
  }

  state = {
  	instructions: {
  		value: "53\n11E RFRFRFRF\n32N FRRFLLFFRRFLL\n03W LLFFFLFLFL",
  		valid: true,
  		showValidations: false,
  		validations: []
  	}
  }

  getValidations = (name, value) => {
  	return compact(validations[name].map(
  		(validation) => {
  			if(validation.name === 'length'){
  				if(value.length < 2 || value.length > 100) {
  					return validation.message
  				}
  			}
  			return null
  		}
  	))
  }

  handleChange = memoize(name => e => {
  	const validations = this.getValidations(name, e.target.value)
  	this.setState({
  		...this.state,
  		[name]: {
  			value: e.target.value,
  			valid: validations.length === 0,
  			validations: validations,
  			showValidations: true
  		}
  	})
  })

  handleSubmit = () => {
  	const {
  		handleSubmit
  	} = this.props

  	handleSubmit({
  		...this.state
  	})
  }

  render () {
  	const {
  		instructions
  	} = this.state

  	return <form>
  		<FormGroup controlId="formControlsTextarea" validationState={instructions.showValidations ? instructions.valid ? null : 'error' : null }>
  		  <ControlLabel>Please write/paste your instructions below</ControlLabel>
  			<FormControl componentClass="textarea" placeholder="textarea" onChange={ this.handleChange('instructions') } value={instructions.value}/>
  			{instructions.validations.map(
  				(message, index) => {
  				    return <HelpBlock key={ index }>
  						{ message }
  					</HelpBlock>
  			   }
  			)}
  		</FormGroup>
  		<FormGroup>
  			<Button bsStyle="primary" onClick={ this.handleSubmit }>Submit</Button>
  		</FormGroup>
  	</form>
  }
}

export default Form
