import React from 'react'
import { Panel } from 'react-bootstrap'

const StepWrapper = ({ children, step, label }) => {
	return   <Panel>
		<Panel.Heading>Part {step} : {label}</Panel.Heading>
		<Panel.Body>{children}</Panel.Body>
	</Panel>
}

export default StepWrapper
