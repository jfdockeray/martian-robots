/**
  * @typedef robotData
  * @type {Object}
  * @property {array<robot>} robots - an array of robots
  * @property {object} bounds - The upper x and y bounds
  */

/**
 * userStringToRobotsData converts user input to robot data
 * @param  {String} str user entered instructions
 * @return {robotData} formated robot data
 */
export function userStringToRobotsData (str) {
	return str.split('\n').reduce((acc, cv, index)=>{
		if(index===0){
			return {
				bounds: {
					x: parseInt(cv[0]),
					y: parseInt(cv[1])
				},
				...acc
			}
		}
		const robotInput = cv.split(" ")
		return {
			...acc,
			robots: [
				...acc.robots,
				{
					position: {
						x: parseInt(robotInput[0][0]),
						y: parseInt(robotInput[0][1]),
						orientation: robotInput[0][2]
					},
					isDead: false,
					instructions: robotInput[1].split('')
				}
			]
		}
	}, {
		robots: []
	})
}
