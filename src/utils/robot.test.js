import { moveRobots, moveRobot, commandRobot } from './robot'

const robotsData = [{
	position: {
		x: 1,
		y: 1,
		orientation: "E"
	},
	isDead: false,
	instructions: ['R', 'F', 'R', 'F', 'R', 'F', 'R', 'F']
}, {
	position: {
		x: 3,
		y: 2,
		orientation: "N"
	},
	isDead: false,
	instructions: ['F', 'R', 'R', 'F', 'L', 'L', 'F', 'F', 'R', 'R', 'F', 'L', 'L']
}, {
	position: {
		x: 0,
		y: 3,
		orientation: "W"
	},
	isDead: false,
	instructions: ['L', 'L', 'F', 'F', 'F', 'L', 'F', 'L', 'F', 'L']
}]

describe('moveRobots', () => {
	test('Returns correct data', () => {
		const robots = moveRobots(robotsData, { bounds: { x: 5, y: 3}})

		expect(robots).toBeDefined()
		expect(robots.length).toBeTruthy()
		expect(robots[0]).toMatch('1 1 E')
		expect(robots[1]).toMatch('3 3 N LOST')
		expect(robots[2]).toMatch('2 3 S')
	})
})

describe('moveRobot', () => {
	test('Returns correct data', () => {
		const robot = moveRobot(robotsData[0], { bounds: { x: 5, y: 3}, deadRobots: []})

		expect(robot).toBeDefined()
		expect(robot).toHaveProperty('position')
		expect(robot).toHaveProperty('instructions', ['R', 'F', 'R', 'F', 'R', 'F', 'R', 'F'])
		expect(robot).toHaveProperty('isDead', false)

		//nested checks on position
		expect(robot.position.x).toEqual(1)
		expect(robot.position.y).toEqual(1)
		expect(robot.position.orientation).toEqual('E')
	})
	test('Returns dead robot', () => {
		const robot = moveRobot(robotsData[1], { bounds: { x: 5, y: 3}, deadRobots: []})

		expect(robot).toBeDefined()
		expect(robot).toHaveProperty('position')
		expect(robot).toHaveProperty('instructions', ['F', 'R', 'R', 'F', 'L', 'L', 'F', 'F', 'R', 'R', 'F', 'L', 'L'])
		expect(robot).toHaveProperty('isDead', true)

		// nested checks on position
		expect(robot.position.x).toEqual(3)
		expect(robot.position.y).toEqual(3)
		expect(robot.position.orientation).toEqual('N')

		// ensure it does not mutate state
		expect(robotsData[1]).toHaveProperty('isDead', false)
	})
	test('Remebers dead robot', () => {
		const luckyRobot = {
			position: {
				x: 0,
				y: 2,
				orientation: "N"
			},
			isDead: false,
			instructions: ['F', 'F', 'F', 'F']
		}
		const robot = moveRobot(luckyRobot, { bounds: { x: 2, y: 2}, deadRobots: [ {x: 0, y: 2} ]})

		expect(robot).toBeDefined()
		expect(robot).toHaveProperty('position')
		expect(robot).toHaveProperty('isDead', false)

		// nested checks on position
		expect(robot.position.x).toEqual(0)
		expect(robot.position.y).toEqual(2)
		expect(robot.position.orientation).toEqual('N')
	})

	test('Test third data example', () => {
		const robot = moveRobot(robotsData[2], { bounds: { x: 5, y: 3}, deadRobots: [ {x: 3, y: 3} ]})

		expect(robot).toBeDefined()
		expect(robot).toHaveProperty('position')
		expect(robot).toHaveProperty('instructions', ['L', 'L', 'F', 'F', 'F', 'L', 'F', 'L', 'F', 'L'])
		expect(robot).toHaveProperty('isDead', false)

		// nested checks on position
		expect(robot.position.x).toEqual(2)
		expect(robot.position.y).toEqual(3)
		expect(robot.position.orientation).toEqual('S')

		// ensure it does not mutate state
		expect(robotsData[1]).toHaveProperty('isDead', false)
	})
})

describe('commandRobot', () => {
	test('Test forward - north', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "N"
		}, 'F')

		expect(position).toHaveProperty('x', 3)
		expect(position).toHaveProperty('y', 4)
	})

	test('Test forward - south', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "S"
		}, 'F')

		expect(position).toHaveProperty('x', 3)
		expect(position).toHaveProperty('y', 2)
	})

	test('Test forward - east', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "E"
		}, 'F')

		expect(position).toHaveProperty('x', 4)
		expect(position).toHaveProperty('y', 3)
	})

	test('Test forward - west', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "W"
		}, 'F')

		expect(position).toHaveProperty('x', 2)
		expect(position).toHaveProperty('y', 3)
	})

	test('Test left - north', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "N"
		}, 'L')

		expect(position).toHaveProperty('orientation', "W")
	})

	test('Test left - west', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "W"
		}, 'L')

		expect(position).toHaveProperty('orientation', "S")
	})

	test('Test left - south', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "S"
		}, 'L')

		expect(position).toHaveProperty('orientation', "E")
	})

	test('Test left - east', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "E"
		}, 'L')

		expect(position).toHaveProperty('orientation', "N")
	})

	test('Test right - north', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "N"
		}, 'R')

		expect(position).toHaveProperty('orientation', "E")
	})

	test('Test right - east', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "E"
		}, 'R')

		expect(position).toHaveProperty('orientation', "S")
	})

	test('Test right - south', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "S"
		}, 'R')

		expect(position).toHaveProperty('orientation', "W")
	})

	test('Test right - west', () => {
		const position = commandRobot({
			x: 3,
			y: 3,
			orientation: "W"
		}, 'R')

		expect(position).toHaveProperty('orientation', "N")
	})
})
