import { userStringToRobotsData } from './io'

describe('moveRobots', () => {
	test('Returns data in the correct shape', () => {
		const robotData = userStringToRobotsData("53\n11E RFRFRFRF\n32N FRRFLLFFRRFLL\n03W LLFFFLFLFL")
		expect(robotData).toHaveProperty('robots')
		expect(robotData).toHaveProperty('bounds')
		expect(robotData.robots).toBeDefined()
		expect(robotData.bounds).toBeDefined()
		expect(robotData.bounds.x).toBe(5)
		expect(robotData.bounds.y).toBe(3)

		expect(robotData.robots[0]).toHaveProperty('position')
		expect(robotData.robots[0]).toHaveProperty('instructions', ['R', 'F', 'R', 'F', 'R', 'F', 'R', 'F'])
		expect(robotData.robots[0]).toHaveProperty('isDead', false)

		expect(robotData.robots[0].position.x).toEqual(1)
		expect(robotData.robots[0].position.y).toEqual(1)
		expect(robotData.robots[0].position.orientation).toEqual('E')

		expect(robotData.robots[1]).toHaveProperty('position')
		expect(robotData.robots[1]).toHaveProperty('instructions', ['F', 'R', 'R', 'F', 'L', 'L', 'F', 'F', 'R', 'R', 'F', 'L', 'L'])
		expect(robotData.robots[1]).toHaveProperty('isDead', false)

		expect(robotData.robots[1].position.x).toEqual(3)
		expect(robotData.robots[1].position.y).toEqual(2)
		expect(robotData.robots[1].position.orientation).toEqual('N')

		expect(robotData.robots[2]).toHaveProperty('position')
		expect(robotData.robots[2]).toHaveProperty('instructions', ['L', 'L', 'F', 'F', 'F', 'L', 'F', 'L', 'F', 'L'])
		expect(robotData.robots[2]).toHaveProperty('isDead', false)

		expect(robotData.robots[2].position.x).toEqual(0)
		expect(robotData.robots[2].position.y).toEqual(3)
		expect(robotData.robots[2].position.orientation).toEqual('W')
	})
})
