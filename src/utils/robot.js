/**
  * @typedef {string} instruction
  *
  */
/**
  * @typedef {string} orientation
  *
  */
/**
  * @typedef position
  * @type {Object}
  * @property {number} x - The x coordinate.
  * @property {number} y - The y coordinate.
  * @property {orientation} orientation - The current orientation of the robot
  *
  */
/**
  * @typedef robot
  * @type {Object}
  * @property {position} position - The current position of the robot
  * @property {instruction[]} instructions - Instructions of how to move the robot
  * @property {Boolean} isDead - did this robot die
  *
  */
/**
  * @typedef boundaries
  * @type {Object}
  * @property {number} x - The x coordinate.
  * @property {number} y - The y coordinate.
  */



/**
  * @enum {instruction}
  */
const INSTRUCTION = {
	LEFT: "L",
	RIGHT: "R",
	FORWARD: "F"
}

const {
	LEFT,
	RIGHT,
	FORWARD
} = INSTRUCTION

/**
  * @enum {orientation}
  */
const ORIENTATION = {
	NORTH: "N",
	SOUTH: "S",
	EAST: "E",
	WEST: "W"
}

const {
	NORTH,
	SOUTH,
	EAST,
	WEST
} = ORIENTATION


/**
 * Executes the commands for each robot in an array
 * @param  {robot[]} robots
 * @param  {Object} options
 * @param {boundaries} options.bounds - Contains the upper x and y of the grid
 * @return {Array<String>} - Final position of each robot
 */
export function moveRobots(robots, options) {
	let deadRobots = []
	return robots.reduce(
		(output, robot) => {
			const movedRobot = moveRobot(robot, {
				...options,
				deadRobots
			})
			if(movedRobot.isDead){
				deadRobots = [...deadRobots, movedRobot.position ]
			}
			return [
				...output,
				robotToString(movedRobot)
			]
		},
		[]
	)
}

/**
 * pretty prints robot
 * @param  {robot}  robot
 * @return {String}
 */
export const robotToString = ({position: { x, y, orientation }, isDead}) => {
	const str = `${x} ${y} ${orientation}`
	return isDead ? `${str} LOST` : str
}

/**
 * moves a robot by executing a series of commands
 *
 * @param  {Object} robot - The current position of the robot
 * @param {Object} options
 * @param {Array<position>} options.deadRobots - Fallen comrades
 * @param {boundaries} options.bounds - Contains the upper x and y of the grid
 * @return {robot} - The final position of the robot
 */
export function moveRobot(robot, options) {
	let isDead = false
	const position = robot.instructions.reduce(
		(activePosition, instruction) => {
			const nextActivePostion = commandRobot(activePosition, instruction)
			if(isDead) {
				return activePosition
			} else if (
				nextActivePostion.x > options.bounds.x ||
				nextActivePostion.y > options.bounds.y
			) { // checks if the next active move is illegal

				const robotGrave = options.deadRobots.reduce(
					(truthy, deadPosition) => {
						if(truthy) return true
						if(activePosition.x === deadPosition.x && activePosition.y === deadPosition.y) {
							return true
						}
						return false
					}, false
				) // check if a robot died already on this square

				isDead = !robotGrave
				return activePosition
			} else {
				// robot is not dead, and next move is legal, so continue
				return nextActivePostion
			}
		},
		robot.position
	)
	return {
		position,
		isDead,
		instructions: robot.instructions
	}
}

/**
 * commandRobot applies a command to a robot
 * @param  {position} position
 * @param  {instruction} command
 * @return {position}
 */
export function commandRobot (position, command) {
	switch(command) {
  	case RIGHT:
  	case LEFT:
  		return {
  			...position,
  			orientation: turn(position.orientation, command === RIGHT)
  		}

  	case FORWARD:
  		return forward(position)

  	default:
  		throw new Error("unrecognised command type")
	}
}

/**
 * turn returns the next orientation based on clockwise/ anti clockwise direction
 * @param  {orientation} currentOrientation
 * @param  {Boolean} clockwise
 * @return {orientation}
 */
export function turn(currentOrientation, clockwise) {
	switch(currentOrientation) {
  	case NORTH:
  		return clockwise ? EAST : WEST
  	case EAST:
  		return clockwise ? SOUTH : NORTH
  	case SOUTH:
  		return clockwise ? WEST : EAST
  	case WEST:
  		return clockwise ? NORTH : SOUTH
  	default:
  		throw new Error("unrecognised orientation type")
  	}
}

/**
 * forward moves the correct coordinate forward based on current orientation
 * @param  {position} position
 * @return {position}
 */
export function forward(position) {
	const {
		orientation,
		x,
		y
	} = position

	switch(orientation) {
	  case NORTH: case SOUTH:
  		return {
			...position,
			y: orientation === SOUTH ? y - 1 : y + 1
		}
  	case EAST: case WEST:
		return {
			...position,
			x: orientation === WEST ? x - 1 : x + 1
		}
  	default:
  		throw new Error("unrecognised orientation type")
  	}
}
