import React, { Component } from 'react'
import {uniqueId} from 'lodash'
import { Alert } from 'react-bootstrap'

import RobotForm from './containers/RobotForm'
import RobotOutput from './containers/RobotOutput'
import AppWrapper from './components/AppWrapper'
import StepWrapper from './components/StepWrapper'
import './App.css';

class App extends Component {
	state = {
		robotData: undefined
	}

	handleSubmit = (robotData) => {
		this.setState({
			robotData: {
				...robotData,
				clientId: uniqueId
			}
		})

	}

	render() {
		const {
			robotData
		} = this.state

		return (
			<AppWrapper>
				<StepWrapper step="1" label="Input">
			      <RobotForm handleSubmit={ this.handleSubmit }/>
				</StepWrapper>
				{ robotData && <StepWrapper step="2" label="Output">
					{ robotData.error ?
						<Alert bsStyle="danger">
						  <strong>Houston we have a problem</strong> Please check your input. This program is designed to work with the
							'\n' character and not carriage returns.
						</Alert> : <RobotOutput robots={robotData.robots} bounds={robotData.bounds} key={robotData.clientId} /> }
				</StepWrapper> }
			</AppWrapper>
		);
	}
}

export default App;
