## Running the application.

**Running the production build files**

I don't normally do this but I have added the build files to the git repo. So all thats needed to run the app is a static server on your local machine (or a web server). For example if you are using [serve](https://www.npmjs.com/package/serve) you can run
`serve -s build` from the root directory. I did this to minimise any possible problems with environments and also as an attempt to satisfy the rule `We should be able to run your code without any crazy steps` .

There aren't any crazy steps to run the the development files or build the app, and this application follows the development process used in `create-react-app`. That said I still felt adding the build files was a worthwhile precautionary step in case there were any problems.

**Project structure**
Please note that documentation can be found for the Logic functions in the `out` folder. If you run `open out` and then open the main `index.html` from you file browser, you can view the documentation.

- **Logic:**
I spent most of the time working on the logic around the robot movement and instructions. I used a TDD approach, designing the interface, writing the tests and then writing the smaller functions first working up to the bigger move functions. These functions and tests are found in `src/utils/robot`.
The other files in utils is `io` and `io.test`, and are the functions for handling user input, and are not meant to be comprehensive.

- **UI:** As this is a very simple app, I have decided not to use any state management libraries and to use React state where state is needed. I've used the standard components and containers React pattern for structure. `Components` contains reusable code related to view, whilst `Containers` contains more specific view logic.
Forms can be tricky, so I have tried to set this up in a way that would scale if it was needed. There is some minimal front end validation in the Forms component to give an idea of how this could be done.

**Environment**
- Node >= 8

**Install**
- `git clone https://jfdockeray@bitbucket.org/jfdockeray/martian-robots.git`
- `npm install` or `yarn`


**Running the development build files**
- `npm start` or `yarn start`

**Running the test**
- `npm test` or `yarn test`

## Available Scripts

In the project directory, you can run:

### `npm run document`
Requires jsdoc and builds the document to out.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


## Learn More
This project is based of the create-react-app tool please find out more from the links below

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
